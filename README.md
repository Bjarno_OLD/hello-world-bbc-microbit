How to run
==========

You need the following packages:

- yotta (https://aur.archlinux.org/packages/yotta)
- ninja (https://www.archlinux.org/packages/community/x86_64/ninja/)
- gcc-arm-none-eabi (https://aur.archlinux.org/packages/gcc-arm-none-eabi-bin/)
- srecord (https://aur.archlinux.org/packages/srecord/)

(Other Linux distributions: check your repositories!)

Run the following commands in order to produce a .hex file:

```
yotta target bbc-microbit-classic-gcc
yotta install lancaster-university/microbit
```

In order to build the .hex file

```
yotta build
```

The produced .hex file can be found in

build/bbc-microbit-classic-gcc/source/hello-world-combined.hex
